// ------------------------------------------------------------
//  Copyright (c) Microsoft Corporation.  All rights reserved.
//  Licensed under the MIT License (MIT). See License.txt in the repo root for license information.
// ------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Fabric;
using System.Fabric.Query;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace VotingWeb.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  public class VotesController : Controller
  {
    private readonly HttpClient httpClient;
    private readonly FabricClient fabricClient;
    private readonly string reverseProxyBaseUri;
    private readonly StatelessServiceContext serviceContext;

    public VotesController(HttpClient httpClient, StatelessServiceContext context, FabricClient fabricClient)
    {
      this.fabricClient = fabricClient;
      this.httpClient = httpClient;
      serviceContext = context;
      reverseProxyBaseUri = Environment.GetEnvironmentVariable("ReverseProxyBaseUri");
    }

    // GET: api/Votes
    [HttpGet("")]
    public async Task<IActionResult> Get()
    {
      Uri serviceName = VotingWeb.GetVotingDataServiceName(serviceContext);
      Uri proxyAddress = GetProxyAddress(serviceName);

      ServicePartitionList partitions = await fabricClient.QueryManager.GetPartitionListAsync(serviceName);

      List<KeyValuePair<string, int>> result = new List<KeyValuePair<string, int>>();

      foreach (Partition partition in partitions)
      {
        string proxyUrl =
            $"{proxyAddress}/api/VoteData?PartitionKey={((Int64RangePartitionInformation)partition.PartitionInformation).LowKey}&PartitionKind=Int64Range";

        using (HttpResponseMessage response = await httpClient.GetAsync(proxyUrl))
        {
          if (response.StatusCode != HttpStatusCode.OK)
          {
            continue;
          }

          result.AddRange(JsonConvert.DeserializeObject<List<KeyValuePair<string, int>>>(await response.Content.ReadAsStringAsync()));
        }
      }

      return Json(result);
    }

    // PUT: api/Votes/name
    [HttpPut("{name}")]
    public async Task<IActionResult> Put(string name)
    {
      Uri serviceName = VotingWeb.GetVotingDataServiceName(serviceContext);
      Uri proxyAddress = GetProxyAddress(serviceName);
      long partitionKey = GetPartitionKey(name);
      string proxyUrl = $"{proxyAddress}/api/VoteData/{name}?PartitionKey={partitionKey}&PartitionKind=Int64Range";

      //string data = $"{{ 'name' : '{name}' }}";
      string data = "{{ 'name' : " + name + ",'occasion':" + DateTime.Now.ToLongTimeString() + " }}";
      //string data = "{{ 'name' : " + name + " }}";
      StringContent putContent = new StringContent(data, Encoding.UTF8, "application/json");
      putContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

      using (HttpResponseMessage response = await httpClient.PutAsync(proxyUrl, putContent))
      {
        return new ContentResult
        {
          StatusCode = (int)response.StatusCode,
          Content = await response.Content.ReadAsStringAsync()
        };
      }
    }

    // DELETE: api/Votes/name
    [HttpDelete("{name}")]
    public async Task<IActionResult> Delete(string name)
    {
      Uri serviceName = VotingWeb.GetVotingDataServiceName(serviceContext);
      Uri proxyAddress = GetProxyAddress(serviceName);
      long partitionKey = GetPartitionKey(name);
      string proxyUrl = $"{proxyAddress}/api/VoteData/{name}?PartitionKey={partitionKey}&PartitionKind=Int64Range";

      using (HttpResponseMessage response = await httpClient.DeleteAsync(proxyUrl))
      {
        if (response.StatusCode != HttpStatusCode.OK)
        {
          return StatusCode((int)response.StatusCode);
        }
      }

      return new OkResult();
    }


    /// <summary>
    /// Constructs a reverse proxy URL for a given service.
    /// Example: http://localhost:19081/VotingApplication/VotingData/
    /// </summary>
    /// <param name="serviceName"></param>
    /// <returns></returns>
    private Uri GetProxyAddress(Uri serviceName)
    {
      return new Uri($"{reverseProxyBaseUri}{serviceName.AbsolutePath}");
    }

    /// <summary>
    /// Creates a partition key from the given name.
    /// Uses the zero-based numeric position in the alphabet of the first letter of the name (0-25).
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private long GetPartitionKey(string name)
    {
      return Char.ToUpper(name.First()) - 'A';
    }
  }
}